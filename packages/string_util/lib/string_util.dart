library string_util;

class StringUtil {
  /// Returns number of words in [value].
  static int countWord(String value) {
    value = value.trim().replaceAll(RegExp(' +'), ' ');
    return value.split(' ').length;
  }

  /// Returns [value] with removed accents.
  static String removeAccents(String value) {
    value = value.trim().replaceAll(RegExp(' +'), ' ');
    final accentsMap = [
      "aàảãáạăằẳẵắặâầẩẫấậ",
      "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
      "dđ",
      "DĐ",
      "eèẻẽéẹêềểễếệ",
      "EÈẺẼÉẸÊỀỂỄẾỆ",
      "iìỉĩíị",
      "IÌỈĨÍỊ",
      "oòỏõóọôồổỗốộơờởỡớợ",
      "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
      "uùủũúụưừửữứự",
      "UÙỦŨÚỤƯỪỬỮỨỰ",
      "yỳỷỹýỵ",
      "YỲỶỸÝỴ"
    ];

    for (var i = 0; i < accentsMap.length; i++) {
      RegExp regex = RegExp('[' + accentsMap[i].substring(1) + ']');
      var char = accentsMap[i][0]; // non-accent letter
      value = value.replaceAllMapped(regex, (match) {
        return char;
      });
    }
    return value;
  }
}
