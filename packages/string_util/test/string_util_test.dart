import 'package:flutter_test/flutter_test.dart';

import 'package:string_util/string_util.dart';

void main() {
  test('Count word tokens in string', () {
    expect(StringUtil.countWord('Nguyễn Văn A'), 3);
  });

  test('Remove string accents', () {
    expect(StringUtil.removeAccents('Nguyễn Văn A'), 'Nguyen Van A');
  });

}
