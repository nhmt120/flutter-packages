import 'package:flutter_test/flutter_test.dart';

import 'package:bignumber_util/bignumber_util.dart';

void main() {
  test('Sum of 2 string values', () {
    expect(BigNumberUtil.thirdGradeSum('123', '345'), '468');
    expect(BigNumberUtil.thirdGradeSum('123456789', '987654321'), '1111111110');
  });
}
