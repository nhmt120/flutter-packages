import 'package:flutter/material.dart';
import 'package:bignumber_util/bignumber_util.dart';
import 'package:string_util/string_util.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();

  final inputController = TextEditingController();
  final num1Controller = TextEditingController();
  final num2Controller = TextEditingController();

  late String num1;
  late String num2;

  String stringUtilResult = '';
  String bigNumberUtilResult = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Test Internal Packages'),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "String Utility Package Test",
                  textScaleFactor: 1.5,
                  style: TextStyle(color: Colors.green),
                ),
              ),
              _buildStringUtilTest(),
              Container(padding: const EdgeInsets.all(10)),
              Text(stringUtilResult),
              Container(padding: const EdgeInsets.all(10)),
              const Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Big Number Utility Package Test",
                  textScaleFactor: 1.5,
                  style: TextStyle(color: Colors.green),
                ),
              ),
              _buildBigNumberUtilTest(),
              Container(padding: const EdgeInsets.all(10)),
              Text(bigNumberUtilResult),
            ],
          ),
        ),
      ),
    );
  }

  _buildBigNumberUtilTest() {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(right: 10),
          width: 150,
          child: TextFormField(
            controller: num1Controller,
            decoration: const InputDecoration(labelText: 'Enter number 1'),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(right: 10),
          width: 150,
          child: TextFormField(
            controller: num2Controller,
            decoration: const InputDecoration(labelText: 'Enter number 2'),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            num1 = num1Controller.text;
            num2 = num2Controller.text;

            String sum = BigNumberUtil.thirdGradeSum(num1, num2);

            bigNumberUtilResult = 'Sum of $num1 and $num2 is $sum';
            setState(() {});
          },
          child: const Text('Test'),
        ),
      ],
    );
  }

  _buildStringUtilTest() {
    late String inputString = '';

    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(right: 10),
          width: 200.0,
          child: TextFormField(
            controller: inputController,
            decoration: const InputDecoration(labelText: 'Enter a string'),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            inputString = inputController.text;
            int count = StringUtil.countWord(inputString);
            String str = StringUtil.removeAccents(inputString);

            stringUtilResult =
                'Number of words: $count\nRemoved accents string: $str';
            setState(() {});
          },
          child: const Text('Test'),
        ),
      ],
    );
  }

  // Widget num1Field() {
  //   return TextFormField(
  //     controller: num1Controller,
  //     decoration: InputDecoration(icon: Icon(Icons.person), labelText: 'input'),
  //     onSaved: (value) {
  //       num1 = value as String;
  //     },
  //   );
  // }

  Widget num2Field() {
    return TextFormField(
      controller: num2Controller,
      decoration: InputDecoration(icon: Icon(Icons.person), labelText: 'input'),
      onSaved: (value) {
        num2 = value as String;
      },
    );
  }

  Widget sumButton() {
    return ElevatedButton(
        onPressed: () async {
          // if (formKey.currentState!.validate()) {
          //   formKey.currentState!.save();
          print("--------------------");
          print('add two number :$num1,$num2');
          BigNumberUtil.thirdGradeSum(num1, num2);
          //Navigator.push(context, MaterialPageRoute(builder: (context) => MapScreen()));
        },
        child: Text('Sum'));
  }

// Widget inputButton() {
//   return ElevatedButton(
//     onPressed: () async {
//       // if (formKey.currentState!.validate()) {
//       //   formKey.currentState!.save();
//       print("--------------------");
//       print('count word: $inputString');
//       StringUtil.countWord(inputString);
//       // convertString.countWord(inputString);
//       print("--------------------");
//       print('normal word: $inputString');
//       StringUtil.removeAccents(inputString);
//       //Navigator.push(context, MaterialPageRoute(builder: (context) => MapScreen()));
//     },
//     child: Text('input'),
//   );
// }
}
